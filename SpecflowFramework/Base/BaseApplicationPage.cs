﻿using OpenQA.Selenium;


namespace AtlassianAssessment.Base
{
    class BaseApplicationPage
    {
        //driver
        public IWebDriver _driver { get; private set; }

        public BaseApplicationPage(IWebDriver driver)
        {
            _driver = driver;
        }
    }

}
